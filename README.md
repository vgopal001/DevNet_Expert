# Cisco DevNet Expert Certification Preparation 

## Introduction

The purpose of creating this repository is to bring all the information and resources in one place regarding the preparation for the DevNet Expert Exam. I will be keep updating this repo with the Python scripts, Ansible Playbooks, Terraform, NSO as well as third-party applications/software (Grafana, Telegraph, Kubernetes etc.) notes. which will certainly help me or other following this repo. Any contribution is also welcome, if you think something is not right and you can fix or have better notes/scripts etc. to upload here. 

## Useful Links to Get Started 

Note: These links are available at the time of writing this readme (May 9, 2022) and the exam version v1.0, however, it may be moved in the future.

* __[Cisco Certified DevNet Expert Certification Overview](https://developer.cisco.com/certification/devnet-expert/)__

* **[Cisco Certified DevNet Expert Lab Exam Format](https://learningnetwork.cisco.com/s/article/devnet-expert-lab-exam-format)**

* **[DevNet Expert (v1.0) Exam Topics – Lab Exam](https://developer.cisco.com/certification/exam-topic-expert/)**

* **[Cisco Certified DevNet Expert (v1.0) Equipment and Software List](https://learningnetwork.cisco.com/s/article/devnet-expert-equipment-and-software-list)**

* **[Cisco Certified DevNet Expert (v1.0) Learning Matrix](https://learningcontent.cisco.com/documents/DevNet_Expert_v1_Learning_Matrix_2.xlsx)**

* **[Cisco Certified DevNet Expert FAQs](https://www.cisco.com/c/dam/en_us/training-events/certifications/devnet-expert-faq.pdf)**

### Cisco DevNet Exam Preparation Links 

* **[Cisco Certified DevNet Expert Certification and Training](https://learningnetwork.cisco.com/s/devnet-expert?tabset-2cc6a=601f6)**

* **[DevNet Expert Prep Program](https://learningnetwork.cisco.com/s/learning-plan-detail-standard?ltui__urlRecordId=a1c6e00000975R4AAI&ltui__urlRedirect=learning-plan-detail-standard&t=1651974055727)**

* **[Cisco DevNet Developer Website](https://developer.cisco.com/)**

* **[Cisco Blogs - Developer](https://blogs.cisco.com/developer)**

* **[Cisco Learning Labs Center](https://developer.cisco.com/learningcenter/)**

* **[Cisco Learning Labs](https://developer.cisco.com/learning/)**

* **[Cisco Digital Learning](https://digital-learning.cisco.com/#/)**

* **[Cisco Learning Network Store](https://learningnetworkstore.cisco.com/cart)**

* **[Cisco DevNet Sandboxes](https://developer.cisco.com/site/sandbox/)**

* **[Cisco DevNet Expert Candidate Work Station (CWS) - OVA](https://cs.co/DevExCWS)**

* **[Cisco DevNet Expert Candidate Work Station (CWS) - QCOW2](https://learningcontent.cisco.com/images/2022-04-08_DevNetExpert_CWS_Example.qcow2)**

* **[CML (Cisco Modeling Labs) - Personal](https://learningnetworkstore.cisco.com/cisco-modeling-labs-personal/cisco-modeling-labs-personal/CML-PERSONAL.html)**

* **[DevNet Certifications Community](https://learningnetwork.cisco.com/s/topic/0TO3i0000008jY5GAI/devnet-certifications-community)**

* **[Cisco Live](https://www.ciscolive.com/global.html)**

### Cisco DevNet Exam Preparation - Recommended Books

* **[Network Programmability and Automation Fundamentals](https://www.ciscopress.com/store/network-programmability-and-automation-fundamentals-9780135183656)**

* **[Network Programmability with YANG: The Structure of Network Automation with YANG, NETCONF, RESTCONF, 
and gNMI](https://www.amazon.com/Network-Programmability-YANG-Structure-Automation-ebook/dp/B07RMK59YC)** 

* **[Network Automation Made Easy](https://www.ciscopress.com/store/network-automation-made-easy-9780137506736)**

* **[Programming and Automating Cisco Networks: A guide to network programmability and automation in the data center, campus, and WAN](https://www.ciscopress.com/store/programming-and-automating-cisco-networks-a-guide-to-9781587144653)**

* **[Containers in Cisco IOS-XE, IOS-XR, and NX-OS: Orchestration and Operation](https://www.ciscopress.com/store/containers-in-cisco-ios-xe-ios-xr-and-nx-os-orchestration-9780135895757)**

* **[Cisco DevNet Professional DEVCOR 350-901 Study Guide](https://www.ciscopress.com/store/cisco-devnet-professional-devcor-350-901-study-guide-9780137500048)**

### Cisco DevNet Exam Preparation - Live Lessons
* **[Practical Python for DevOps Engineers LiveLessons (Video Training)](https://www.ciscopress.com/store/practical-python-for-devops-engineers-livelessons-video-9780137659005)**

* **[Kubernetes in the Data Center LiveLessons](https://www.ciscopress.com/store/kubernetes-in-the-data-center-livelessons-9780135646496)**

### DevNet Expert Lab Exam Booking 

* **[CCIE Lab and CCDE Practical Exam Locations](https://www.cisco.com/c/en/us/training-events/training-certifications/certifications/expert/ccie-lab-exam-locations.html)**

* **[CCIE Mobile Lab Location and Schedule](https://learningnetwork.cisco.com/s/mobile-lab-scheduler)**

* **[Book CCIE Lab or CCDE Practical Exam](https://ccie.cloudapps.cisco.com/CCIE/Schedule_Lab/CCIEOnline/CCIEOnline)**

* **[CCIE BYOD Mobile Labs](https://learningnetwork.cisco.com/s/byod-mobile-labs-overview)**

#### Lab Exam Cost
* The exam fee is $1900 USD for the CCIE lab and CCDE practical exams conducted in a Cisco testing facility and at mobile labs using the Cisco kit option.

* The exam fee is $1600 USD for the CCIE lab and CCDE practical exams at mobile labs using the Bring Your Own Device (BYOD)option.

### DevNet Expert Candidate Work Station (CWS) Specifications 

The candidate workstation is based on Ubuntu 20.04 LTS. Documentation for software will be supplied where available.

The following software is installed on the candidate workstation:

* Ubuntu 20.04 standard GNOME desktop
	* Python 3.9
	* Ansible 2.9.26
		* cisco.aci 2.1
	* PyCharm 2021
	* Vim 8.1
	* Atom 1.58
* Visual Studio Code 1.60
	* Plugins:
		* Docker
		* GitLens – Git supercharged
		* HashiCorp Terraform
		* Python
		* Pylance
		* Remote Development
		* Remote – Containers
		* Remote – SSH
		* YAML
		* Better Jinja
		* Yangster
		* XML Tools
	* Firefox
	* Google Chrome
	* Docker 20.10 + compose plugin v2.2
	* Docker-compose 1.29
	* Terraform 1.0
		* ciscodevnet/aci v0.7
	* Git 2.25
	* Kubectl 1.23
	* telnet
	* OpenSSH 8.2
	* cURL 7.68
	* wget 1.20
	* netcat
	* Postman 9
	* Wireshark 3.2
	* OpenSSL 1.1
	* Nginx 1.18
	* Bash 5.0
	* Zsh 5.8
	* Tcsh 6.21
	* Jq 1.6
	* Xmllint
	* direnv 2.21
	* Cisco Network Services Orchestrator (NSO) local installation 5.5
	* Cisco NSO IOS CLI NED 6.69
	* Cisco NSO NX-OS CLI NED 5.21
	* Cisco YANG Suite 2.8

For Python libraries required for the CWS, you can download and pip install via [requirements.txt](https://gitlab.com/muhammadrafi/DevNet_Expert/-/blob/main/requirements.txt) file. 

### IOS Images and Virtual Machines 

* Ubuntu Linux: 20.04 LTS
* IOSvL2: 15.2
* IOSv: 15.9
* Cisco Catalyst 8000V: 17.5
* Nexus 9300v (N9Kv): 9.3(8)
* Cisco Application Policy Infrastructure Controller 5.2 w/ ACI Simulator

Note: You can download these images from [here](https://software.cisco.com/download/home
), if you have a valid Cisco contract.

### Third Party Software Links

* **[GitLab 14](https://about.gitlab.com/gitlab-14/)**
* **[GitLab Runner 14](https://docs.gitlab.com/runner/)**
* **[HashiCorp Vault 1.8](https://www.vaultproject.io/docs/release-notes/1.8.0)**
* **[Telegraf 1.19](https://www.influxdata.com/blog/release-announcement-telegraf-1-19-0/)**
* **[InfluxDB 1.8](https://docs.influxdata.com/influxdb/v1.8/about_the_project/releasenotes-changelog/)**
* **[Grafana 8.1](https://grafana.com/docs/grafana/latest/whatsnew/whats-new-in-v8-1/)**
* **[Kubernetes 1.23](https://kubernetes.io/releases/)**

## Author
[Muhammad Rafi](https://www.linkedin.com/in/muhammad-rafi-0a37a248/)

## References
*https://learningnetwork.cisco.com/s/article/devnet-expert-equipment-and-software-list*
